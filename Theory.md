1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
document.createElement(tagName): Цей метод створює новий DOM-елемент з вказаним тегом.
element.appendChild(childElement): Цей метод додає дочірній елемент в кінець батьківського елемента.
parentElement.insertBefore(newElement, referenceElement): Цей метод вставляє новий елемент перед 
вказаним елементом-посиланням всередині батьківського елемента. 


2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
Знайти елемент з класом "navigation"(document.querySelector(), document.getElementsByClassName()). 
Перевірити, чи знайдено елемент перед спробою видалення. Використати метод remove() для видалення 
елементу зі сторінки.


3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
insertBefore(newElement, referenceElement): Цей метод вставляє новий елемент перед вказаним 
елементом-посиланням всередині батьківського елемента.
insertAdjacentElement(position, newElement): Цей метод дозволяє вставляти новий елемент в певну 
позицію відносно вибраного елемента.
