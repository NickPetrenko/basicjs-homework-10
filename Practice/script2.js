//2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
//Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
//Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
//Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
//Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

document.addEventListener("DOMContentLoaded", function() {
    var select = document.createElement('select');
    select.setAttribute('id', 'rating');
  
    var option1 = document.createElement('option');
    option1.value = '4';
    option1.textContent = '4 Stars';
  
    var option2 = document.createElement('option');
    option2.value = '3';
    option2.textContent = '3 Stars';
  
    var option3 = document.createElement('option');
    option3.value = '2';
    option3.textContent = '2 Stars';
  
    var option4 = document.createElement('option');
    option4.value = '1';
    option4.textContent = '1 Star';
  
    select.appendChild(option1);
    select.appendChild(option2);
    select.appendChild(option3);
    select.appendChild(option4);
  
    var main = document.querySelector('main');
    var featuresSection = document.querySelector('#features');
  
    main.insertBefore(select, featuresSection);
});