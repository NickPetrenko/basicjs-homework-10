//1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут 
//href з посиланням на "#". Додайте цей елемент в footer після параграфу.

document.addEventListener("DOMContentLoaded", function() {
    var footer = document.querySelector('footer');
    var link = document.createElement('a');
    link.textContent = 'Learn More';
    link.setAttribute('href', '#');
    var paragraph = footer.querySelector('p');
    footer.insertBefore(link, paragraph.nextSibling);
});